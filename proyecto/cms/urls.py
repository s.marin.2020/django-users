from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('loggedIn', views.loggedIn),
    path('logout', views.logout_views),
    # se pone aqui para que no se ejecute la view de get
    # content ya que tambien es un string, y se
    # evaluan estas entradas atendiendo al orden.
    path('<str:llave>', views.get_content),
]
